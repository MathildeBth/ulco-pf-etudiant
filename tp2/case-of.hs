fibo :: Int -> Int 
fibo n = case n of
            0 -> 0
            1 -> 1
            x -> fibo (n-1) + fibo (n-2)

main :: IO()
main = do
    print (fibo 9)
    print (fibo 10)
    print (fibo 11)