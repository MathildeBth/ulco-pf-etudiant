formaterParite :: Int -> String
formaterParite n =
    if even n
        then "pair"
        else "impair"

formaterSigne :: Int -> String
formaterSigne n =
    if n == 0
    then "nul"
    else if n < 0
        then "negatif"
        else "positif"

main :: IO ()
main = do
    putStrLn(formaterParite 21)
    putStrLn(formaterParite 42)
    putStrLn(formaterSigne 0)
    putStrLn(formaterSigne (-42))
    putStrLn(formaterSigne 42)