safeSqrt :: Double -> Maybe Double
safeSqrt x = if x < 0
             then Nothing
             else Just (sqrt x)

formatedSqrt :: Double -> String
formatedSqrt x = "sqrt(" ++ show x ++ ") " ++ res
    where res = case safeSqrt of
                    Nothing -> "is not defined"
                    Just r -> "= " ++ show r

main :: IO ()
main = do
    print (safeSqrt 16)
    print (safeSqrt (-16))
    putStrLn (formatedSqrt 16)
    putStrLn (formatedSqrt (-16))