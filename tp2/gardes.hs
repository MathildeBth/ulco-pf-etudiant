formaterPariteGarde :: Int -> String
formaterPariteGarde n 
    | even n = "pair"
    | otherwise = "impair"

formaterSigneGarde :: Int -> String
formaterSigneGarde n =
    | n == 0 = "null"
    | n > 0 = "positif"
    | otherwise = "positif"

main :: IO ()
main = do
    putStrLn(formaterPariteGarde 21)
    putStrLn(formaterPariteGarde 42)