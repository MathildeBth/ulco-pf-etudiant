xor1 :: Bool -> Bool -> Bool
xor1 a b = (a && not b) || (not a && b)

xor2 :: Bool -> Bool -> Bool
xor2 a b = if a then not b else b 

xor3 :: Bool -> Bool -> Bool
xor3 a b = case (a.b) of
            (True, True) -> False
            (False, False) -> False
            _ -> True

xor4 :: Bool -> Bool -> Bool
xor4 a b
    | a == b = False
    | otherwise = true

xor5 :: Bool -> Bool -> Bool
xor5 True True = False
xor5 False False = False
xor5 _ _ = True

testXor :: (Bool -> Bool -> Bool) -> Bool
textXor f 
    = True `f` True == False
    && False `f` False == False
    && False `f` False == True
    && False `f` True == True

main :: IO ()
main = do
    putStrLn "\nxor1"
    print (testXor xor1)

    putStrLn "\nxor2"
    print (testXor xor2)

    putStrLn "\nxor3"
    print (testXor xor3)

    putStrLn "\nxor4"
    print (testXor xor4)

    putStrLn "\nxor5"
    print (testXor xor5)


