import Text.Read

main :: IO ()
main = do
    line <- getLine
    let x = read line 
    print (x::Int)
    putStrLn "this is the end"
