import Text.Read
import System.Environment
import System.IO

type Task = (Int, Bool, String)     -- id, done?, description
type Todo = (Int, [Task])           -- nextId, tasks

--affichage des tâches 
affichageTODOS :: Task -> IO ()
affichageTODOS (n, b, todo) = do
    if (b == True) then
        putStrLn("[X] " ++ show id ++ ". " ++ show txt)
    else
        putStrLn("[-] " ++ show id ++ ". " ++ show txt)

affichageDO :: [Task] -> IO ()
affichageDO [] = putStrLn("")
affichageDO  ((n, b, todo) : reste) = do
    if (b == False) then
         affichageTODOS (n, b, todo)
    else
        affichageDONE reste

affichageDONE :: [Task] -> IO ()
affichageDONE [] = putStrLn("")
affichageDONE  ((n, b, todo) : reste) = do
    if (b == True) then
        affichageTODOS (n, b, todo)
    else
        affichageDO reste

--correspondance du menu avec les fonctions
correspondanceMenu :: Todo -> IO ()
putStr ("> ")
correspondanceMenu    "print" -> affichageTODO todo
    "print todo" -> affichageTODOS todo
    "print done" -> 

    if (line /= "exit") then correspondanceMenu todo
    

main :: IO ()
main = do
    putStrLn ("Commandes :")
    putStrLn ("print")
    putStrLn ("print todo")
    putStrLn ("print done")
    putStrLn ("add <string>")
    putStrLn ("do <int>")
    putStrLn ("undo <int>")
    putStrLn ("del <int>")
    putStrLn ("exit")
    contents <- readFile "tasks.txt"
    let todo1 = read $! contents :: Todo
    print todo1
    mapM_ print (snd todo1)