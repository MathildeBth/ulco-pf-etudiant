import System.IO
import System.Random

-- Liste des mots à deviner
motAleatoire :: [String]
motAleatoire = ["trotinette", "voiture", "train"]

-- Avancer du pendu
etape :: Int -> String
etape 0 =   " ___________________ "
etape 1 =   "     |               \n\
            \     |               \n\
            \     |               \n\
            \     |               \n\
            \     |               \n\
            \ ____|______________ "
etape 2 =   "     __________      \n\
            \     |               \n\
            \     |               \n\
            \     |               \n\
            \     |               \n\
            \     |               \n\
            \ ____|______________ "
etape 3 =   "     __________      \n\
            \     |/              \n\
            \     |               \n\
            \     |               \n\
            \     |               \n\
            \     |               \n\
            \ ____|______________ "
etape 4 =   "     __________      \n\
            \     |/        |     \n\
            \     |               \n\
            \     |               \n\
            \     |               \n\
            \     |               \n\
            \ ____|______________ " 
etape 5 =   "     __________      \n\
            \     |/        |     \n\
            \     |        (_)    \n\
            \     |               \n\
            \     |               \n\
            \     |               \n\
            \ ____|______________ "
etape 6 =   "     __________      \n\
            \     |/        |     \n\
            \     |        (_)    \n\
            \     |         |     \n\
            \     |         |     \n\
            \     |               \n\
            \ ____|______________ "
etape 7 =   "     __________      \n\
            \     |/        |     \n\
            \     |        (_)    \n\
            \     |       \\|/    \n\
            \     |         |     \n\
            \     |               \n\
            \ ____|______________ "
etape 8 =   "     __________      \n\
            \     |/        |     \n\
            \     |        (_)    \n\
            \     |       \\|/    \n\
            \     |         |     \n\
            \     |        / \    \n\
            \ ____|______________ "

-- Mot à deviner
motSecret :: String
motSecret = "trotinette"

-- Lire une ligne du clavier
-- Affiche à l'écran chaque caractère comme un ?
lireLigne :: IO String
lireLigne = do 
    x <- obtenirChar
    if x == '\n' then
        do
            putChar x
            return []
        else
            do
                putChar '?'
                xs <- lireLigne
                return ( x : xs )

-- Lire un caractère du clavier sans l'afficher
obtenirChar :: IO Char
obtenirChar = do 
    hSetEcho stdin False
    c <- getChar
    hSetEcho stdin True
    return c

-- Demande un mot
-- Compare le mot avec le mot à deviner
deviner :: String -> IO ()
deviner_7 = putStrLn("C'est perdu")
deviner mot = do
    --print(etape n)
    putStr " Lettre : "
    xs <- getLine
    if xs == mot then
        putStrLn " Bien joué ’!"
    else
        do
            putStrLn ( match mot xs )
            deviner mot

-- Montre les caractère qui appartiennent à l'autre chaîne
match :: String -> String -> String
match xs ys =
   [if elem x ys 
        then x 
    else '?' | x <- xs]

main :: IO ()
main = do
    --indice <- randomRIO (0,2)
    --putStrLn $ motAleatoire !! indice
    putStrLn " Essayez de le deviner :"
    deviner motSecret
    