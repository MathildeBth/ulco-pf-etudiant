mapDoubler1 :: [Int] -> [Int]
mapDoubler1 [] = []
mapDoubler1 (x:xs) = x*2 : mapDoubler1 xs

mapDoubler2 :: [Int] -> [Int]
mapDoubler2 [] = []
mapDoubler2 xs = map (*2) xs

mymap :: (Int -> Int) -> [Int] -> [Int]
mymap _ [] = []
mymap f (x:xs) = f x : mymap f xs

main :: IO ()
main = do
    print (mapDoubler1 [1..4])
    print (mapDoubler2 [1..4])
    print (mymap (*2) [1..4])