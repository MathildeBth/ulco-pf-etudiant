import Data.Char

filterEven :: [Int] -> [Int]
filterEven [] = []
filterEven (x:xs) = 
    if even x 
    then x:filterEven xs 
    else filterEven xs

filterEven2 :: [Int] -> [Int]
filterEven2 xs = filter even xs

myfilter :: (a -> Bool) -> [a] -> [a]
myfilter _ [] = []
myfilter p (x:xs) =
    if p x
    then x : myfilter p xs
    else myfilter p xs

main :: IO ()
main = do
    print (filterEven [1..4])
    print (filterEven2 [1..4])
    print (myfilter even [1..4])
    print (myfilter isLetter "mathilde456")