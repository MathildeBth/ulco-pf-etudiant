import Data.Char (ord,chr)

doubler :: [Int] -> [Int]
doubler xs = [ x*2 | x<-xs ]

pairs :: [Int] -> [Int]
pairs xs = [ x | x<-xs, even x ]

mymap :: (a -> b) -> [a] -> [b]
mymap f xs = [ f x | x<-xs]

myfilter :: (a -> Bool) -> [a] -> [a]
myfilter p xs = [ x | x<-xs, p x]

multiples :: Int -> [Int]
multiples n = [ x | x<-[1..n], n `mod` x == 0 ]

combinaisons :: [a] -> [b] -> [(a,b)]
combinaisons l1 l2 = [(x,y) | x<-l1, y<-l2]

tripletsPyth :: Int -> [(Int,Int,Int)]
tripletsPyth n = [(x, y, z) | x <- [1 .. n], y <- [x .. n], z <- [1 .. n], (x * x) + (y * y) == (z * z)]

decaler :: Int -> Char -> Char
decaler n x = chr(ord x+n)

chiffrerCesar :: Int -> String -> String
chiffrerCesar n (x:xs) = decaler n x : chiffrerCesar n xs

main :: IO ()
main = do
    print (doubler [1..4])
    print (pairs [1..4])
    print (mymap (*2) [1..4])
    print (myfilter even [1..4])
    print (multiples 35)
    print (combinaisons ["pomme" , "poire"] [13 , 37 , 42])
    print (tripletsPyth 13)
    print (chiffrerCesar 1 "mathilde")
