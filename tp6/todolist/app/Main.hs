-- nix-shell
-- cabal build pour compiler
-- cabal run todolist pour excecuter
-- exit pour quitter nix-shell
-- cabal clean
{-# LANGUAGE OverloadedStrings #-}

import qualified GI.Gtk as Gtk
import Data.Text

handleButton :: IO ()
handleButton = putStrLn "hello"

main :: IO ()
main = do
    _ <- Gtk.init Nothing
    window <- Gtk.windowNew Gtk.WindowTypeToplevel 
    Gtk.windowSetDefaultSize window 300 100
    Gtk.windowSetTitle window "Hello World!"
    _ <- Gtk.onWidgetDestroy window Gtk.mainQuit

    --label <- Gtk.labelNew (Just "Hello World!")
    --Gtk.containerAdd window label

    box <- Gtk.boxNew Gtk.OrientationVertical 1
    Gtk.containerAdd window box

    button <- Gtk.buttonNewWithLabel ("Ajouter")
    Gtk.containerAdd window button
    _ <- Gtk.onButtonClicked button handleButton

    Gtk.widgetShowAll window
    Gtk.main

