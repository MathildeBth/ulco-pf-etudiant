myTake2 :: [Int] -> [Int]
--myTake2 (xs) = take 2 xs
myTake2 = take 2

myGet2 :: [Int] -> Int
myGet2 =  flip (!!) 2

main :: IO ()
main = do
    print (myTake2 [1..4])
    print (myGet2 [1..4])
    print (map (2*) [1..4])
    print (map ((*) 2) [1..4])