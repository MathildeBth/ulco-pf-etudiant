main :: IO ()
main = do
    putStrLn "lamba"
    print (map (\x -> x*2) [1..4::Int])
    print (map (\x ->x `div` 2) [1..4::Int])
    print (map (\(c,i) -> [c] ++ "-" ++ show i) (zip ['a'..'z'] [1..]))
    print (zipWith (\c i -> [c] ++ "-" ++ show i) ['a'..'z'] [1..])
    print (zipWith (\c -> \i -> [c] ++ "-" ++ show i) ['a'..'z'] [1..])