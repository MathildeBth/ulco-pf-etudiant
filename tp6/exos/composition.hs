plus42Positif :: Int -> Bool
plus42Positif = (>0) . (+42)

nul2PlusUn :: Int -> Int
nul2PlusUn = (+1) . (*2)

nul2MoinsUn :: Int -> Int
nul2MoinsUn = (flip (-) 1) . (*2)

applyTwice :: (a -> a) -> a -> a
applyTwice f = f . f

main :: IO ()
main = do
    putStrLn "composition"
    print (plus42Positif 2)
    print (plus42Positif (-84))
    print (nul2PlusUn 10)
    print (nul2MoinsUn 10)
    print (applyTwice nul2PlusUn 10)