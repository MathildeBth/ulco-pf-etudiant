loopEcho :: Int -> IO String
loopEcho 0 = return "loop terminated"
loopEcho n = do
    putStr "> "
    line <- getLine
    if null line
    then return "empty line"
    else do 
        putStrLn line
        loopEcho (n - 1)

main :: IO ()
main = do
    res <- loopEcho 3
    putStrLn res