mylast :: [a] -> a
mylast (x : []) = x
mylast (x: xs) = mylast xs
mylast [] = error "empty list"

myinit :: [a] -> [a]
myinit (_ : xs) = xs
myinit [] = error "empty list"

myreplicate :: Int -> a -> [a]
myreplicate 0 _ = []
myreplicate n x = x : myreplicate (n -1) x

mydrop :: Int -> [a] -> [a]
mydrop 0 xs = xs
mydrop _ [] = []
mydrop n (_ : xs) = mydrop (n -1) xs

mytake :: Int -> [a] -> [a]
mytake 0 _ = []
mytake _ [] = []
mytake n (x : xs) = x : mytake (n -1) xs

main :: IO ()
main = do
    print (mylast "foobar")