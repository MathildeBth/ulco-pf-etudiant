fuzzyLenght :: [a] -> String
fuzzyLenght [] = "empty"
fuzzyLenght [_] = "one"
fuzzyLenght [_,_] = "two"
fuzzyLenght _ = "many"

main :: IO ()
main = do
    putStrLn (fuzzyLenght [])
    putStrLn (fuzzyLenght ['a'])
    putStrLn (fuzzyLenght ['a','t'])
    putStrLn (fuzzyLenght ['a','t','t'])