safeHead :: [a] -> Maybe a
safeHead [] = Nothing
safeHead (x : _) = Just x

safeTail :: [a] -> Maybe [a]
safeTail [] = Nothing
safeTail (_ : xs) = Just xs

main :: IO ()
main = do
    print( safeHead "")
    print( safeHead "foobar")
    print( safeTail "")
    print( safeTail "foobar")