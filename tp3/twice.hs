twice :: [a] -> [a]
twice xs = xs ++ xs

sym :: [a] -> [a]
sym xs = xs ++ reverse xs

main :: IO ()
main = do
    print (twice [1,2])
    print (twice "to")
    print (sym [1,2])
    print (sym "to")