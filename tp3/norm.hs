dot :: [Double] -> [Double] -> Double
dot xs ys = sum (zipWith (*) xs ys)

norm :: [Double] -> Double
norm xs = sqrt (dot xs xs)

main :: IO ()
main = do
    print (dot [3, 4] [1, 2])
    print (dot [3, 4] [3, 4])
    print (norm [3, 4])