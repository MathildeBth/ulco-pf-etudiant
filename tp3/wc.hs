import System.Environment

compterLignes :: String -> Int
compterLignes str = length (lines str) 

compterMots :: String -> Int
compterMots str = length (words str)

compterCaracteres :: String -> Int
compterCaracteres str = length str

compterCaracteresNonVides :: String -> Int
compterCaracteresNonVides str = length (concat (words str))

main :: IO ()
main = do
    args <- getArgs
    case args of
        [filename] -> do
            -- lire fichier
            content <- readFile filename
            print (compterLignes content)
            print (compterMots content)
            print (compterCaracteres content)
            print (compterCaracteresNonVides content)
        _ -> putStrLn "usage: <file>"
