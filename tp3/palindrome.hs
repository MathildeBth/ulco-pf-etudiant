palindrome :: String -> Bool
palindrome word = word == reverse word

main :: IO ()
main = do
    print(palindrome"radar")
    print(palindrome"allo")