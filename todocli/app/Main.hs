import Text.Read
import System.Environment
import System.IO

type Task = (Int, Bool, String)     -- id, done?, description
type Todo = (Int, [Task])           -- nextId, tasks

--affichage des tâches 
affichageTODOS :: Task -> IO ()
affichageTODOS (n, b, todo) = do
    if (b == True) then
        --tache terminé
        putStrLn("[X] " ++ show n ++ ". " ++ show todo)
    else
        --tache à faire
        putStrLn("[-] " ++ show n ++ ". " ++ show todo)

affichageDO :: [Task] -> IO ()
affichageDO [] = putStrLn("")
affichageDO  ((n, b, todo) : reste) = do
    if (b == False) then
         affichageTODOS (n, b, todo)
    else
        affichageDONE reste

affichageDONE :: [Task] -> IO ()
affichageDONE [] = putStrLn("")
affichageDONE  ((n, b, todo) : reste) = do
    if (b == True) then
        affichageTODOS (n, b, todo)
    else
        affichageDO reste

todoDONE :: Int -> Todo -> Todo
todoDONE num (idtodo, ((n, b, todo) : reste)) = do
    if(n == num) then
        (idtodo, [(n, True, todo)])
    else
        todoDONE num (idtodo, reste)

launchAffichage :: [Task] -> IO ()
launchAffichage [] = putStrLn("")
launchAffichage (a : reste) = do
    affichageTODOS a
    launchAffichage reste

--correspondance du menu avec les fonctions
correspondanceMenu :: Todo -> IO ()
correspondanceMenu todo = do
    putStr ("> ")
    line <- getLine

    if (line == "print") then
        launchAffichage (snd(todo) :: [Task])
    else if (line == "print todo") then
        affichageDO (snd(todo) :: [Task])
    else if (line == "print done") then
        affichageDONE (snd(todo) :: [Task])
    else if (line /= "exit") then 
        correspondanceMenu todo
    else 
        putStrLn ("erreur") 

main :: IO ()
main = do
    putStrLn ("Commandes :")
    putStrLn ("print")
    putStrLn ("print todo")
    putStrLn ("print done")
    putStrLn ("add <string>")
    putStrLn ("do <int>")
    putStrLn ("undo <int>")
    putStrLn ("del <int>")
    putStrLn ("exit")
    contents <- readFile "tasks.txt"
    let todo1 = read $! contents :: Todo
    correspondanceMenu todo1
    print todo1
    mapM_ print (snd todo1)