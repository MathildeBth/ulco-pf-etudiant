main :: IO ()
main = do
    putStr "Quel est ton nom ? "
    name <- getLine
    if(name == "")
        then   
            putStrLn "GoodBye!"
        else do
            putStrLn ("Hello "++name++"!")
            main