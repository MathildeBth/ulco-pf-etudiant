import System.Random

loop :: Int -> Int -> IO()
loop target 0 = putStrLn "fin"
loop target n = do
    putStr ("Type a number (" ++n++ " tries) : ")
    line <- getLine
    let x = read line :: Int
    if x > target then putStr "Too Big!"
    else if x < target then putStrLn "Too Low!"
    else putStrLn "you win!"

    loop target (n-1)

main :: IO()
main = do
    target <- randomRIO (0,100 :: Int)
    loop target 10